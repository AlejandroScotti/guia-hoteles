$(function () {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $(".carousel").carousel({
    interval: 1500,
  });

  $("#contacto").on("show.bs.modal", function (e) {
     console.log("El modal se está mostrando"),
        $("#contactoBtn").removeClass("btn-outline-success");
    $("#contactoBtn").addClass("btn-info");
    $("#contactoBtn").prop("disable", true);
  });
  $("#contacto").on("shown.bs.modal", function (e) {
    console.log("El modal se mostró");
  });
  $("#contacto").on("hide.bs.modal", function (e) {
    console.log("El modal se oculta");
  });
  $("#contacto").on("hidden.bs.modal", function (e) {
    console.log("El modal se ocultó");
    $("#contactoBtn").removeClass("btn-info");
    $("#contactoBtn").addClass("btn-outline-success");
    $("#contactoBtn").prop("disable", false);
  });

  $("#contacto").on("show.bs.modal", function (e) {
    console.log("El modal se está mostrando");
    $("#contactoBtn2").removeClass("btn-outline-success");
    $("#contactoBtn2").addClass("btn-info");
    $("#contactoBtn2").prop("disable", true);
  });
  $("#contacto").on("shown.bs.modal", function (e) {
    console.log("El modal se mostró");
  });
  $("#contacto").on("hide.bs.modal", function (e) {
    console.log("El modal se oculta");
  });
  $("#contacto").on("hidden.bs.modal", function (e) {
    console.log("El modal se ocultó");
    $("#contactoBtn2").removeClass("btn-info");
    $("#contactoBtn2").addClass("btn-outline-success");
    $("#contactoBtn2").prop("disable", false);
  });
});
